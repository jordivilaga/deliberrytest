current-dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

.PHONY: build
build: deps start

.PHONY: deps
deps: composer-install

# Composer
composer-env-file:
	@if [ ! -f .env.local ]; then echo '' > .env.local; fi

.PHONY: composer-install
composer-install: CMD=install

.PHONY: composer-update
composer-update: CMD=update

.PHONY: composer-require
composer-require: CMD=require
composer-require: INTERACTIVE=-ti --interactive

.PHONY: composer-require-module
composer-require-module: CMD=require $(module)
composer-require-module: INTERACTIVE=-ti --interactive

.PHONY: composer
composer composer-install composer-update composer-require composer-require-module: composer-env-file
	@docker run --rm $(INTERACTIVE) --volume $(current-dir):/app --user $(id -u):$(id -g) \
		composer:2 $(CMD) \
			--ignore-platform-reqs \
			--no-ansi

.PHONY: reload
reload: composer-env-file
	@docker-compose exec php-fpm kill -USR2 1
	@docker-compose exec nginx nginx -s reload

.PHONY: test
test: composer-env-file
	docker exec deliberry-deliberry_test-catalog-php ./vendor/bin/phpunit --testsuite catalog
#	docker exec deliberry-deliberry_test-catalog-php ./vendor/bin/behat -p catalog_behat
	docker exec deliberry-deliberry_test-catalog-php ./vendor/bin/behat -p catalog_behat --format=progress -v

# Docker Compose
.PHONY: start
start: CMD=up --build -d

.PHONY: stop
stop: CMD=stop

.PHONY: destroy
destroy: CMD=down

.PHONY: doco
doco start stop destroy: composer-env-file
	@docker-compose $(CMD)

.PHONY: rebuild
rebuild: composer-env-file
	docker-compose build --pull --force-rm --no-cache
	make deps
	make start

.PHONY: ping-mysql
ping-mysql:
	@docker exec deliberry-deliberry_test-catalog-mysql mysqladmin --user=root --password= --host "127.0.0.1" ping --silent

.PHONY: clean-cache
clean-cache:
	@rm -rf applications/*/*/var
	@docker exec deliberry-deliberry_test-catalog-php ./applications/catalog/bin/console cache:warmup

# Doctrine
.PHONY: doctrine-install
doctrine-install:
	@docker exec deliberry-deliberry_test-catalog-php ./applications/catalog/bin/console doctrine:database:create

.PHONY: doctrine-make-migration
doctrine-make-migration:
	@docker exec deliberry-deliberry_test-catalog-php ./applications/catalog/bin/console make:migration

.PHONY: doctrine-migrate
doctrine-migrate:
	@docker exec deliberry-deliberry_test-catalog-php ./applications/catalog/bin/console doctrine:migrations:migrate