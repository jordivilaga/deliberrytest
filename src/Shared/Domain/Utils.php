<?php
declare(strict_types=1);

namespace Deliberry\Shared\Domain;

use RuntimeException;

final class Utils
{
    public static function deserialize(string $json)
    {
        $data = json_decode($json, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new RuntimeException('Unable to parse response body into JSON: ' . json_last_error());
        }

        return $data;
    }
}