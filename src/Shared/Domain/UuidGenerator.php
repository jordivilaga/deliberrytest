<?php
declare(strict_types=1);

namespace Deliberry\Shared\Domain;

interface UuidGenerator
{
    public function generate(): string;
}