<?php
declare(strict_types=1);

namespace Deliberry\Shared\Domain\ValueObject;

use Ramsey\Uuid\Uuid as RamseyUuid;
use Stringable;
use InvalidArgumentException;

class Uuid implements Stringable
{
    public function __construct(private string $value)
    {
        $this->ensureIsValidUuid($value);
    }

    public static function random(): self
    {
        return new static(RamseyUuid::uuid4()->toString());
    }

    public function value(): string
    {
        return $this->value;
    }

    public function __toString(): string
    {
        return $this->value();
    }

    private function ensureIsValidUuid(string $id): void
    {
        if (!RamseyUuid::isValid($id)) {
            throw new InvalidArgumentException(sprintf('<%s> does not allow the value <%s>.', static::class, $id));
        }
    }
}