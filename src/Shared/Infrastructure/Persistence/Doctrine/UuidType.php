<?php
declare(strict_types=1);

namespace Deliberry\Shared\Infrastructure\Persistence\Doctrine;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

abstract class UuidType extends StringType
{
    abstract protected function getCustomName(): string;

    public function getName(): string
    {
        return $this->getCustomName();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $className = $this->getCustomName();

        return new $className($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->value();
    }
}