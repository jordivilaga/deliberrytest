<?php
declare(strict_types=1);

namespace Deliberry\Shared\Infrastructure\Bus\Command;

use Deliberry\Shared\Domain\Bus\Command\Command;
use Deliberry\Shared\Domain\Bus\Command\CommandBus;
use Deliberry\Shared\Domain\Bus\Command\CommandNotRegisteredException;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\NoHandlerForMessageException;
use Symfony\Component\Messenger\MessageBusInterface;

final class MessengerCommandBus implements CommandBus
{
    public function __construct(private MessageBusInterface $bus)
    {
    }

    public function dispatch(Command $command): void
    {
        try {
            $this->bus->dispatch($command);
        } catch (NoHandlerForMessageException) {
            throw new CommandNotRegisteredException($command);
        } catch (HandlerFailedException $error) {
            throw $error->getPrevious() ?? $error;
        }
    }
}