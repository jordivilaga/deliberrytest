<?php
declare(strict_types=1);

namespace Deliberry\Shared\Infrastructure\Bus\Query;

use Deliberry\Shared\Domain\Bus\Query\Query;
use Deliberry\Shared\Domain\Bus\Query\QueryBus;
use Deliberry\Shared\Domain\Bus\Query\QueryNotRegisteredException;
use Deliberry\Shared\Domain\Bus\Query\QueryResponse;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\Exception\NoHandlerForMessageException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

final class MessengerQueryBus implements QueryBus
{
    public function __construct(private MessageBusInterface $queryBus)
    {
    }

    public function ask(Query $query): ?QueryResponse
    {
        try {
            $handledStamp = $this->queryBus->dispatch($query)->last(HandledStamp::class);

            return $handledStamp->getResult();
        } catch (NoHandlerForMessageException) {
            throw new QueryNotRegisteredException($query);
        } catch (HandlerFailedException $error) {
            throw $error->getPrevious() ?? $error;
        }
    }
}