<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Create;

use Deliberry\Catalog\Products\Domain\ProductDescription;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductName;
use Deliberry\Shared\Domain\Bus\Command\CommandHandler;

final class CreateProductCommandHandler implements CommandHandler
{
    public function __construct(private ProductCreator $creator)
    {
    }

    public function __invoke(CreateProductCommand $command): void
    {
        $id = new ProductId($command->id());
        $name = new ProductName($command->name());
        $description = new ProductDescription($command->description());

        $this->creator->__invoke($id, $name, $description);
    }
}