<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Create;

use Deliberry\Catalog\Products\Domain\Product;
use Deliberry\Catalog\Products\Domain\ProductDescription;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductName;
use Deliberry\Catalog\Products\Domain\ProductRepository;

final class ProductCreator
{
    public function __construct(private ProductRepository $repository)
    {
    }

    public function __invoke(ProductId $id, ProductName $name, ProductDescription $description)
    {
        $product = new Product($id, $name, $description);

        $this->repository->save($product);
    }
}