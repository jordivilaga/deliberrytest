<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Delete;

use Deliberry\Catalog\Products\Application\Find\ProductFinder;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductRepository;
use function Lambdish\Phunctional\apply;

final class ProductDeletion
{
    private ProductFinder $finder;

    public function __construct(private ProductRepository $repository)
    {
        $this->finder = new ProductFinder($repository);
    }

    public function __invoke(ProductId $id): void
    {
        $product = apply($this->finder, [$id]);

        $this->repository->delete($product);
    }
}