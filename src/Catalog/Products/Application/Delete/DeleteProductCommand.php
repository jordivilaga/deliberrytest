<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Delete;

use Deliberry\Shared\Domain\Bus\Command\Command;

final class DeleteProductCommand implements Command
{
    public function __construct(private string $id)
    {
    }

    public function id(): string
    {
        return $this->id;
    }
}