<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Delete;

use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Shared\Domain\Bus\Command\CommandHandler;

final class DeleteProductCommandHandler implements CommandHandler
{
    public function __construct(private ProductDeletion $deletion)
    {
    }

    public function __invoke(DeleteProductCommand $command): void
    {
        $id = new ProductId($command->id());

        $this->deletion->__invoke($id);
    }
}