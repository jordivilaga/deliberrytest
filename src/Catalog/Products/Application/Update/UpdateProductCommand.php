<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Update;

use Deliberry\Shared\Domain\Bus\Command\Command;

final class UpdateProductCommand implements Command
{
    public function __construct(private string $id, private string $name, private string $description)
    {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function description(): string
    {
        return $this->description;
    }


}