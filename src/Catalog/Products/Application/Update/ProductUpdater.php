<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Update;

use Deliberry\Catalog\Products\Application\Find\ProductFinder;
use Deliberry\Catalog\Products\Domain\ProductDescription;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductName;
use Deliberry\Catalog\Products\Domain\ProductRepository;
use function Lambdish\Phunctional\apply;

final class ProductUpdater
{
    private ProductFinder $finder;

    public function __construct(private ProductRepository $repository)
    {
        $this->finder = new ProductFinder($this->repository);
    }

    public function __invoke(ProductId $id, ProductName $name, ProductDescription $description)
    {
        $product = apply($this->finder, [$id]);
        $product->update($name, $description);

        $this->repository->save($product);
    }
}