<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Update;

use Deliberry\Catalog\Products\Domain\ProductDescription;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductName;
use Deliberry\Shared\Domain\Bus\Command\CommandHandler;

final class UpdateProductCommandHandler implements CommandHandler
{
    public function __construct(private ProductUpdater $updater)
    {
    }

    public function __invoke(UpdateProductCommand $command): void
    {
        $id = new ProductId($command->id());
        $name = new ProductName($command->name());
        $description = new ProductDescription($command->description());

        $this->updater->__invoke($id, $name, $description);
    }
}