<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Find;

use Deliberry\Catalog\Products\Application\ProductResponseConverter;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Shared\Domain\Bus\Query\QueryHandler;
use function Lambdish\Phunctional\apply;
use function Lambdish\Phunctional\pipe;

final class FindProductQueryHandler implements QueryHandler
{
    private $finder;

    public function __construct(ProductFinder $finder)
    {
        $this->finder = pipe($finder, new ProductResponseConverter());
    }

    public function __invoke(FindProductsQuery $query)
    {
        $id = new ProductId($query->id());

        return apply($this->finder, [$id]);
    }
}