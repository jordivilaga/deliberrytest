<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Find;

use Deliberry\Shared\Domain\Bus\Query\Query;

final class FindProductsQuery implements Query
{
    public function __construct(private string $id)
    {
    }

    public function id(): string
    {
        return $this->id;
    }
}