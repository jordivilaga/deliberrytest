<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application\Find;

use Deliberry\Catalog\Products\Domain\Product;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductNotFoundException;
use Deliberry\Catalog\Products\Domain\ProductRepository;

final class ProductFinder
{
    public function __construct(private ProductRepository $repository)
    {
    }

    public function __invoke(ProductId $id): Product
    {
        $product = $this->repository->search($id);

        if (null === $product) {
            throw new ProductNotFoundException();
        }

        return $product;
    }
}