<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Application;

use Deliberry\Catalog\Products\Domain\Product;

final class ProductResponseConverter
{
    public function __invoke(Product $product): ProductResponse
    {
        return new ProductResponse(
            $product->id()->value(),
            $product->name()->value(),
            $product->description()->value()
        );
    }
}