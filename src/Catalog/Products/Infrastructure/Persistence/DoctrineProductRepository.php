<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Infrastructure\Persistence;

use Deliberry\Catalog\Products\Domain\Product;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductRepository;
use Deliberry\Shared\Infrastructure\Persistence\Doctrine\DoctrineRepository;

final class DoctrineProductRepository extends DoctrineRepository implements ProductRepository
{
    public function save(Product $product): void
    {
        $this->persist($product);
    }

    public function search(ProductId $id): ?Product
    {
        return $this->repository(Product::class)->find($id);
    }

    public function delete(Product $product): void
    {
        $this->remove($product);
    }
}