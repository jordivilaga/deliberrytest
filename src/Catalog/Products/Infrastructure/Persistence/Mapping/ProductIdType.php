<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Infrastructure\Persistence\Mapping;

use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Shared\Infrastructure\Persistence\Doctrine\UuidType;

final class ProductIdType extends UuidType
{
    protected function getCustomName(): string
    {
        return ProductId::class;
    }
}