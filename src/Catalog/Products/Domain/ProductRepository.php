<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Domain;

interface ProductRepository
{
    public function save(Product $product): void;

    public function search(ProductId $id): ?Product;

    public function delete(Product $product): void;
}