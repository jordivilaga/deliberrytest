<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Domain;

use Deliberry\Shared\Domain\ValueObject\Uuid;

final class ProductId extends Uuid
{
}