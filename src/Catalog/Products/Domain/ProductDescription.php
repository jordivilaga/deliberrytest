<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Domain;

use Deliberry\Shared\Domain\ValueObject\StringValueObject;

final class ProductDescription extends StringValueObject
{
}