<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Domain;

final class ProductNotFoundException extends \Exception
{
}