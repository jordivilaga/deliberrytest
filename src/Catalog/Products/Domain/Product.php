<?php
declare(strict_types=1);

namespace Deliberry\Catalog\Products\Domain;

use Deliberry\Shared\Domain\AggregateRoot;

final class Product implements AggregateRoot
{
    public function __construct(private ProductId $id, private ProductName $name, private ProductDescription $description)
    {
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function name(): ProductName
    {
        return $this->name;
    }

    public function description(): ProductDescription
    {
        return $this->description;
    }

    public function update(ProductName $name, ProductDescription $description): void
    {
        $this->name = $name;
        $this->description = $description;
    }
}