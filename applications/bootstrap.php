<?php

use Symfony\Component\Dotenv\Dotenv;

$rootPath = dirname(__DIR__);

require $rootPath.'/vendor/autoload.php';

if (file_exists($rootPath.'/config/bootstrap.php')) {
    require $rootPath.'/config/bootstrap.php';
} elseif (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv($rootPath.'/.env');
}
