<?php
declare(strict_types=1);

namespace Deliberry\Applications\Catalog\Controller\HealthCheck;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class HealthCheckGetController
{
    public function __invoke(): Response
    {
        return new JsonResponse(
            [
                'status' => 'ok'
            ],
            Response::HTTP_OK
        );
    }
}