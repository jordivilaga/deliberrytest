<?php
declare(strict_types=1);

namespace Deliberry\Applications\Catalog\Controller\Products;

use Deliberry\Catalog\Products\Application\Create\CreateProductCommand;
use Deliberry\Shared\Domain\Bus\Command\CommandBus;
use Deliberry\Shared\Domain\UuidGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ProductPostController
{
    public function __construct(private CommandBus $commandBus, private UuidGenerator $generator)
    {
    }

    public function __invoke(Request $request): Response
    {
        $this->commandBus->dispatch(
            new CreateProductCommand(
                $this->generator->generate(),
                $request->request->getAlpha('name'),
                $request->request->getAlpha('description')
            )
        );

        return new Response('', Response::HTTP_CREATED);
    }
}