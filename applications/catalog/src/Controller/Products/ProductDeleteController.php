<?php
declare(strict_types=1);

namespace Deliberry\Applications\Catalog\Controller\Products;

use Deliberry\Catalog\Products\Application\Delete\DeleteProductCommand;
use Deliberry\Shared\Domain\Bus\Command\CommandBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ProductDeleteController
{
    public function __construct(private CommandBus $commandBus)
    {
    }

    public function __invoke(string $id, Request $request): Response
    {
        $this->commandBus->dispatch(new DeleteProductCommand($id));

        return new Response('', Response::HTTP_OK);
    }
}