<?php
declare(strict_types=1);

namespace Deliberry\Applications\Catalog\Controller\Products;

use Deliberry\Catalog\Products\Application\Update\UpdateProductCommand;
use Deliberry\Shared\Domain\Bus\Command\CommandBus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ProductPutController
{
    public function __construct(private CommandBus $commandBus)
    {
    }

    public function __invoke(string $id, Request $request): Response
    {
        $this->commandBus->dispatch(
            new UpdateProductCommand(
                $id,
                $request->get('name'),
                $request->get('description')
            )
        );

        return new Response('', Response::HTTP_OK);
    }
}