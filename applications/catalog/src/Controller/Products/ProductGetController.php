<?php
declare(strict_types=1);

namespace Deliberry\Applications\Catalog\Controller\Products;

use Deliberry\Catalog\Products\Application\Find\FindProductsQuery;
use Deliberry\Catalog\Products\Application\ProductResponse;
use Deliberry\Shared\Domain\Bus\Query\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ProductGetController
{
    public function __construct(private QueryBus $queryBus)
    {
    }

    public function __invoke(string $id, Request $request): Response
    {
        /** @var ProductResponse $response */
        $response = $this->queryBus->ask(new FindProductsQuery($request->get('id')));

        return new JsonResponse(
            [
                'id' => $response->id(),
                'name' => $response->name(),
                'description' => $response->description()
            ],
            Response::HTTP_OK
        );
    }
}