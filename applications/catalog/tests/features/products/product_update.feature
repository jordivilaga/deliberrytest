Feature: Updates an existing product
  In order to be able to update products on the platform
  As a user with admin permissions
  I want to update an existing product

  Scenario: An existing product
    Given An existing product with:
    """
    {
      "id": "cdd79cb4-9149-443f-aafc-1eb29cf8ccc4",
      "name": "Name",
      "description": "Description"
    }
    """
    When I send a PUT request to "/products/cdd79cb4-9149-443f-aafc-1eb29cf8ccc4" with body:
    """
    {
      "name": "NewName",
      "description": "NewDescription"
    }
    """
    Then the response status code should be 200
    And the response should be empty
