Feature: Create a new product
  In order to have products on the platform
  As a user with admin permissions
  I want to create a new product

  Scenario: A valid non existing product
    Given I send a POST request to "/products" with body:
    """
    {
      "name": "Product 1",
      "description": "Lorem ipsum dolor sit amet"
    }
    """
    Then the response status code should be 201
    And the response should be empty
