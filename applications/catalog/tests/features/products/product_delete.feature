Feature: Deletes a product product
  In order to be able to delete products on the platform
  As a user with admin permissions
  I want to delete an existing product

  Scenario: An existing product
    Given An existing product with id "cdd79cb4-9149-443f-aafc-1eb29cf8ccc1"
    When I send a DELETE request to "/products/cdd79cb4-9149-443f-aafc-1eb29cf8ccc1"
    Then the response status code should be 200
    And the response should be empty
