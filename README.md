## Environment Setup

### Introduction

1. [Install Docker](https://www.docker.com/get-started)
2. Clone this project: `git clone https://gitlab.com/jordivilaga/deliberrytest.git`
3. Move to the project folder: `cd DeliberryTest`

### Application execution

1. Install all the dependencies and bring up the project with Docker executing: `make build`
2. Then you'll have all the endpoints available (1 HealthCheck and 4 API endpoints):
    2.1. Catalog Health Check: http://localhost:8030/health-check

### Tests execution

1. Execute PHPUnit and Behat tests: `make test`