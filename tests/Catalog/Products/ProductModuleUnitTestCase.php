<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products;

use Deliberry\Catalog\Products\Domain\Product;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductRepository;
use Deliberry\Tests\Shared\Infrastructure\UnitTest\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;

abstract class ProductModuleUnitTestCase extends UnitTestCase
{
    private ProductRepository|MockObject|null $repository;

    protected function shouldSave(Product $product): void
    {
        $this->repository()
            ->expects($this->once())
            ->method('save')
            ->with($product);
    }

    protected function shouldSearch(ProductId $id, ?Product $product): void
    {
        $this->repository()
            ->expects($this->once())
            ->method('search')
            ->with($id)
            ->willReturn($product);
    }

    protected function repository(): ProductRepository|MockObject
    {
        return $this->repository = $this->repository ?? $this->mock(ProductRepository::class);
    }
}