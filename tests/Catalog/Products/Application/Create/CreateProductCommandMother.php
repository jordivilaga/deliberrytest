<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Application\Create;

use Deliberry\Catalog\Products\Application\Create\CreateProductCommand;
use Deliberry\Catalog\Products\Domain\ProductDescription;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductName;
use Deliberry\Tests\Catalog\Products\Domain\ProductDescriptionMother;
use Deliberry\Tests\Catalog\Products\Domain\ProductIdMother;
use Deliberry\Tests\Catalog\Products\Domain\ProductNameMother;

final class CreateProductCommandMother
{
    public static function create(
        ?ProductId $id = null,
        ?ProductName $name = null,
        ?ProductDescription $description = null
    ): CreateProductCommand {
        return new CreateProductCommand(
            $id?->value() ?? ProductIdMother::create()->value(),
            $name?->value() ?? ProductNameMother::create()->value(),
            $description?->value() ?? ProductDescriptionMother::create()->value()
        );
    }
}