<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Application\Create;

use Deliberry\Catalog\Products\Application\Create\CreateProductCommandHandler;
use Deliberry\Catalog\Products\Application\Create\ProductCreator;
use Deliberry\Tests\Catalog\Products\Domain\ProductMother;
use Deliberry\Tests\Catalog\Products\ProductModuleUnitTestCase;

final class CreateProductCommandHandlerTest extends ProductModuleUnitTestCase
{
    private CreateProductCommandHandler|null $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $this->handler = new CreateProductCommandHandler(new ProductCreator($this->repository()));
    }

    /** @test */
    public function it_should_create_a_valid_product(): void
    {
        $command = CreateProductCommandMother::create();

        $product = ProductMother::fromRequest($command);

        $this->shouldSave($product);

        $this->dispatch($command, $this->handler);
    }
}