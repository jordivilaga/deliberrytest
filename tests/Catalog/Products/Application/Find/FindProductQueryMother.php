<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Application\Find;

use Deliberry\Catalog\Products\Application\Find\FindProductsQuery;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Tests\Catalog\Products\Domain\ProductIdMother;

final class FindProductQueryMother
{
    public static function create(
        ?ProductId $id = null
    ): FindProductsQuery {
        return new FindProductsQuery(
            $id?->value() ?? ProductIdMother::create()->value()
        );
    }
}