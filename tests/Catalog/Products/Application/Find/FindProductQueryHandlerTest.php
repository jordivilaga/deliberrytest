<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Application\Find;

use Deliberry\Catalog\Products\Application\Find\FindProductQueryHandler;
use Deliberry\Catalog\Products\Application\Find\ProductFinder;
use Deliberry\Catalog\Products\Domain\ProductNotFoundException;
use Deliberry\Tests\Catalog\Products\Domain\ProductIdMother;
use Deliberry\Tests\Catalog\Products\Domain\ProductMother;
use Deliberry\Tests\Catalog\Products\ProductModuleUnitTestCase;

final class FindProductQueryHandlerTest extends ProductModuleUnitTestCase
{
    private FindProductQueryHandler|null $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $this->handler = new FindProductQueryHandler(new ProductFinder($this->repository()));
    }

    /** @test */
    public function it_should_find_an_existing_product(): void
    {
        $product = ProductMother::create();

        $query = FindProductQueryMother::create($product->id());

        $this->shouldSearch($product->id(), $product);

        $this->ask($query, $this->handler);
    }

    /** @test */
    public function it_should_throw_an_exception_when_find_a_product_does_not_exists(): void
    {
        $this->expectException(ProductNotFoundException::class);

        $id = ProductIdMother::create();
        $query = FindProductQueryMother::create($id);

        $this->shouldSearch($id, null);

        $this->ask($query, $this->handler);
    }
}