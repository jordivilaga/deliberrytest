<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Application\Update;

use Deliberry\Catalog\Products\Application\Update\UpdateProductCommand;
use Deliberry\Catalog\Products\Domain\ProductDescription;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductName;
use Deliberry\Tests\Catalog\Products\Domain\ProductDescriptionMother;
use Deliberry\Tests\Catalog\Products\Domain\ProductIdMother;
use Deliberry\Tests\Catalog\Products\Domain\ProductNameMother;

final class UpdateProductCommandMother
{
    public static function create(
        ?ProductId $id = null,
        ?ProductName $name = null,
        ?ProductDescription $description = null
    ): UpdateProductCommand {
        return new UpdateProductCommand(
            $id?->value() ?? ProductIdMother::create()->value(),
            $name?->value() ?? ProductNameMother::create()->value(),
            $description?->value() ?? ProductDescriptionMother::create()->value()
        );
    }
}