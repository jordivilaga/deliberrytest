<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Application\Update;

use Deliberry\Catalog\Products\Application\Update\ProductUpdater;
use Deliberry\Catalog\Products\Application\Update\UpdateProductCommandHandler;
use Deliberry\Catalog\Products\Domain\ProductNotFoundException;
use Deliberry\Tests\Catalog\Products\Domain\ProductIdMother;
use Deliberry\Tests\Catalog\Products\Domain\ProductMother;
use Deliberry\Tests\Catalog\Products\ProductModuleUnitTestCase;

final class UpdateProductCommandHandlerTest extends ProductModuleUnitTestCase
{
    private UpdateProductCommandHandler|null $handler;

    protected function setUp(): void
    {
        parent::setUp();

        $this->handler = new UpdateProductCommandHandler(new ProductUpdater($this->repository()));
    }

    /** @test */
    public function it_should_update_an_existing_product(): void
    {
        $command = UpdateProductCommandMother::create();

        $product = ProductMother::fromRequest($command);

        $this->shouldSearch($product->id(), $product);
        $this->shouldSave($product);

        $this->dispatch($command, $this->handler);
    }

    /** @test */
    public function it_should_throw_an_exception_when_the_product_does_not_exists(): void
    {
        $this->expectException(ProductNotFoundException::class);

        $command = UpdateProductCommandMother::create();

        $product = ProductMother::fromRequest($command);

        $this->shouldSearch($product->id(), null);

        $this->dispatch($command, $this->handler);
    }
}