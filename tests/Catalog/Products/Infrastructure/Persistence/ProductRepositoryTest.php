<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Infrastructure\Persistence;

use Deliberry\Tests\Catalog\Products\Domain\ProductIdMother;
use Deliberry\Tests\Catalog\Products\Domain\ProductMother;
use Deliberry\Tests\Catalog\Products\ProductModuleInfrastructureTestCase;

final class ProductRepositoryTest extends ProductModuleInfrastructureTestCase
{
    /** @test */
    public function it_should_save_a_product(): void
    {
        $product = ProductMother::create();

        $this->repository()->save($product);
    }

    /** @test */
    public function it_should_return_an_existing_product(): void
    {
        $product = ProductMother::create();

        $this->repository()->save($product);

        $this->assertEquals($product, $this->repository()->search($product->id()));
    }

    /** @test */
    public function it_should_not_return_a_non_existing_product(): void
    {
        $this->assertNull($this->repository()->search(ProductIdMother::create()));
    }
}