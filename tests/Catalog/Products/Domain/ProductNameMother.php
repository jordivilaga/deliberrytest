<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Domain;

use Deliberry\Catalog\Products\Domain\ProductName;
use Deliberry\Tests\Shared\Domain\StringMother;

final class ProductNameMother
{
    public static function create(?string $value = null): ProductName
    {
        return new ProductName($value ?? StringMother::create());
    }
}