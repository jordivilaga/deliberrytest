<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Domain;

use Deliberry\Catalog\Products\Domain\ProductDescription;
use Deliberry\Tests\Shared\Domain\StringMother;

final class ProductDescriptionMother
{
    public static function create(?string $value = null): ProductDescription
    {
        return new ProductDescription($value ?? StringMother::create());
    }
}