<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Domain;

use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Tests\Shared\Domain\UuidMother;

final class ProductIdMother
{
    public static function create(?string $value = null): ProductId
    {
        return new ProductId($value ?? UuidMother::create());
    }
}