<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products\Domain;

use Deliberry\Catalog\Products\Application\Create\CreateProductCommand;
use Deliberry\Catalog\Products\Application\Update\UpdateProductCommand;
use Deliberry\Catalog\Products\Domain\Product;
use Deliberry\Catalog\Products\Domain\ProductDescription;
use Deliberry\Catalog\Products\Domain\ProductId;
use Deliberry\Catalog\Products\Domain\ProductName;

final class ProductMother
{
    public static function create(
        ?ProductId $id = null,
        ?ProductName $name = null,
        ?ProductDescription $description = null
    ): Product {
        return new Product(
            $id ?? ProductIdMother::create(),
            $name ?? ProductNameMother::create(),
            $description ?? ProductDescriptionMother::create()
        );
    }

    public static function fromRequest(CreateProductCommand|UpdateProductCommand $request): Product
    {
        return self::create(
            ProductIdMother::create($request->id()),
            ProductNameMother::create($request->name()),
            ProductDescriptionMother::create($request->description())
        );
    }
}