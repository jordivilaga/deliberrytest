<?php
declare(strict_types=1);

namespace Deliberry\Tests\Catalog\Products;

use Deliberry\Applications\Catalog\Kernel;
use Deliberry\Catalog\Products\Domain\ProductRepository;
use Deliberry\Tests\Shared\Infrastructure\UnitTest\InfrastructureTestCase;

abstract class ProductModuleInfrastructureTestCase extends InfrastructureTestCase
{
    protected function repository(): ProductRepository
    {
        return $this->service(ProductRepository::class);
    }

    protected function kernelClass(): string
    {
        return Kernel::class;
    }
}