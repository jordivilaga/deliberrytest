<?php
declare(strict_types=1);

namespace Deliberry\Tests\Shared\Domain;

final class StringMother
{
    public static function create(): string
    {
        return MotherCreator::random()->word;
    }
}