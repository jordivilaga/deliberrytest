<?php
declare(strict_types=1);

namespace Deliberry\Tests\Shared\Infrastructure\UnitTest;

use Deliberry\Shared\Domain\Bus\Command\Command;
use Deliberry\Shared\Domain\Bus\Query\Query;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

abstract class UnitTestCase extends TestCase
{
    protected function mock(string $className): MockObject
    {
        return $this->createMock($className);
    }

    protected function dispatch(Command $command, callable $commandHandler): void
    {
        $commandHandler($command);
    }

    protected function ask(Query $query, callable $queryHandler): void
    {
        $queryHandler($query);
    }
}