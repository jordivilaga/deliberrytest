<?php
declare(strict_types=1);

namespace Deliberry\Tests\Shared\Infrastructure\Behat;

use Behat\Gherkin\Node\PyStringNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Deliberry\Catalog\Products\Application\Create\CreateProductCommand;
use Deliberry\Shared\Domain\Bus\Command\CommandBus;
use Deliberry\Shared\Domain\Utils;
use RuntimeException;

final class ApplicationContext extends RawMinkContext
{
    public function __construct(private CommandBus $bus)
    {
    }

    /**
     * @Given An existing product with id :id
     */
    public function iCreateAProductWithId($id): void
    {
        $this->bus->dispatch(
            new CreateProductCommand(
                $id,
                'The best product',
                'Lorem ipsum dolor sit amet'
            )
        );
    }

    /**
     * @Given /^An existing product with:$/
     */
    public function iCreateAProductWithData(PyStringNode $json): void
    {
        $data = Utils::deserialize($json->getRaw());
        $requiredData = ['id', 'name', 'description'];

        if (count(array_diff( array_keys($data), $requiredData )) > 0) {
            throw new RuntimeException('Unable to create the product because there are not existing attributes');
        }

        $this->bus->dispatch(
            new CreateProductCommand(
                $data['id'],
                $data['name'],
                $data['description']
            )
        );
    }
}